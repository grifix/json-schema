Simple json schema generator
# Installation
`composer require grifix/json-schema`

# Usage
```php
JsonSchema::create()->generate(SomeClass::class) //returns array with SomeClass json schema
```
