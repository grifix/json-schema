<?php

declare(strict_types=1);

namespace Grifix\JsonSchema;

final class JsonSchema
{
    public static function create(): self
    {
        return new self();
    }

    public function generate(string $class): array
    {
        $result = [
            'type' => 'object',
            'additionalProperties' => false,
            'required' => [],
            'properties' => []
        ];

        $reflection = new \ReflectionClass($class);
        foreach ($reflection->getProperties() as $property) {
            if (!$property->getType()->allowsNull()) {
                $result['required'][] = $property->getName();
            }

            $result['properties'][$property->getName()] = [
                'type' => $this->getType($property)
            ];
        }

        return $result;
    }

    private function getType(\ReflectionProperty $property): string|array
    {
        $result = $property->getType()->getName();
        if (!$property->getType()->isBuiltin()) {
            $result = 'object';
        }
        if ($result === 'bool') {
            $result = 'boolean';
        }
        if ($result === 'float') {
            $result = 'number';
        }
        if ($result === 'int') {
            $result = 'integer';
        }
        if ($property->getType()->allowsNull()) {
            $result = [
                $result,
                'null'
            ];
        }
        return $result;
    }
}
