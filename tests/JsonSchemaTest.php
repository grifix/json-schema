<?php

declare(strict_types=1);

namespace Grifix\JsonSchema\Tests;

use Grifix\JsonSchema\JsonSchema;
use Grifix\JsonSchema\Tests\Stub\Car;
use PHPUnit\Framework\TestCase;

final class JsonSchemaTest extends TestCase
{
    public function testItCreates(): void
    {
        self::assertEquals(
            [
                'type' => 'object',
                'additionalProperties' => false,
                'required' =>
                    [
                        0 => 'id',
                        1 => 'dateOfCreation',
                        2 => 'number',
                        3 => 'price',
                        4 => 'components',
                        5 => 'isNew',
                        6 => 'engine',
                    ],
                'properties' =>
                    [
                        'id' =>
                            [
                                'type' => 'string',
                            ],
                        'dateOfCreation' =>
                            [
                                'type' => 'object',
                            ],
                        'number' =>
                            [
                                'type' => 'integer',
                            ],
                        'price' =>
                            [
                                'type' => 'number',
                            ],
                        'components' =>
                            [
                                'type' => 'array',
                            ],
                        'isNew' =>
                            [
                                'type' => 'boolean',
                            ],
                        'engine' =>
                            [
                                'type' => 'object',
                            ],
                        'note' =>
                            [
                                'type' =>
                                    [
                                        0 => 'string',
                                        1 => 'null',
                                    ],
                            ],
                    ],
            ],
            JsonSchema::create()->generate(Car::class)
        );
    }
}
