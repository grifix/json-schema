<?php

declare(strict_types=1);

namespace Grifix\JsonSchema\Tests\Stub;

final class Component
{

    public function __construct(
        public readonly string $name,
        public readonly string $description
    )
    {
    }
}
