<?php

declare(strict_types=1);

namespace Grifix\JsonSchema\Tests\Stub;

final class Car
{
    /**
     * @param Component[] $components
     */
    public function __construct(
        public readonly string $id,
        public readonly \DateTimeImmutable $dateOfCreation,
        public readonly int $number,
        public readonly float $price,
        public readonly array $components,
        public readonly bool $isNew,
        public readonly Engine $engine,
        public readonly ?string $note = null
    ) {
    }
}
