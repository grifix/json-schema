<?php

declare(strict_types=1);

namespace Grifix\JsonSchema\Tests\Stub;

final class Engine
{
    public function __construct(
        public readonly string $type,
        public readonly string $volume
    )
    {
    }
}
